// const knex = require('knex')({
//     client: 'mysql2',
//     connection: {
//         host: 'hcmus-web.database.windows.net',
//         user: 'adminsql@hcmus-web',
//         password: 'web_2021',
//         port: 3306,
//         database: "bdt",
//     },
// });

// module.exports = knex;
const knex = require('knex')({
    client: 'mysql2',
    connection: {
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database: 'bdt',
        port: 3306
    },
    pool: {
        min: 0,
        max: 50
    }
});

module.exports = knex;